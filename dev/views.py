from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from django.db import connection,connections
from django.conf import settings
from geopy.geocoders import Nominatim
from datetime import datetime,timezone,timedelta
from dateutil import tz
from itertools import islice
from plotly import offline
# from zoneinfo import ZoneInfo
from backports.zoneinfo import ZoneInfo

from .models import GSMSQL_UPS,GFRID_UPS,Version_UPS,TrackerCommands_ver,Alerts

import folium,pytz,haversine as hs,plotly.graph_objects as go,datetime as dt,plotly.offline as opy,timeago,json
import datetime as datet, random, pandas as pd
import plotly.graph_objects as go

class create_dict(dict):
    # __init__ function 
    def __init__(self): 
        self = dict() 
          
    # Function to add key:value 
    def add(self, key, value): 
        self[key] = value

# Create your views here.
@login_required(login_url='/Gateway/login_auth/')
def Home(request):
    return render(request,'index.html')

def login_auth(request):
    return render(request, 'login.html')

@csrf_exempt
def Dev(request):
    username = request.POST.get('gfrnew_username')
    password = request.POST.get('gfrnew_password')
    request.session['gfrnew_user'] = username 
    request.session['gfrnew_pass'] = password

    if username == "admin" and password == "pwd@admin"  or 'gfrnew_user' in request.session:
        return render(request,'index.html')
    else :
        messages = "Invalid Credential"
        return render(request, 'login.html',{'error_message':messages})

def OBD(request):
    return render(request,'obd.html')

def CAN(request):
    return render(request,'can.html')

def RobotArm(request):
    return render(request,'robotarm.html')

def SmartMotor(request):
    return render(request,'smartmotor.html')

def SmartValue(request):
    return render(request,'smartvalue.html')

def Camera(request):
    return render(request,'camera.html')

def Logout(request):
    return HttpResponseRedirect('/Gateway/')

##################################################### UPS ########################################################

def Ups(request):
    data = []
    ld = []
    lt = []
    gsm = []
    astat =[]
    ls = ['JIVSJIUPSUPS2517','JIVSJIUPSUPS6387','JIVSJIUPSUPS1097']
    cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
    for i in range(len(ls)):
        g_id = GFRID_UPS.objects.get(tracker=ls[i]).id
        with connections['ups'].cursor() as cursor1:
            cursor1.execute(f"SELECT * FROM ups_ups WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
            da = cursor1.fetchall()
        
        if da[0][11] != None:
            if '1' in str(da[0][11][:-1]) or da[0][11][-1] == '0':  
                astat.append(1) 
            elif('0' in str(da[0][11][:-1])) and da[0][11][-1] == '1':
                astat.append(0)
        else:
            astat.append(da[0][11])

        g = GSMSQL_UPS.objects.filter(GFRID=g_id).last()
        gsm.append(timeago.format(datetime.fromtimestamp(g.TS).strftime("%Y-%m-%d %H:%M:%S"),cur))
        ld.append(da[0][6])
        lt_t = datetime.fromtimestamp(da[0][10]).strftime("%Y-%m-%d %H:%M:%S")
        lt.append(timeago.format(lt_t,cur))                                    
        print(lt,ld)
        
        geolocator = Nominatim(user_agent="my_application")
        loc = [(12.9815,80.2180),(13.0067, 80.2206)]
        trac = ["UPS-101","UPS-201","UPS-301","UPS-401"]
        mydict = create_dict()
        m = folium.Map(location=[13.0827,80.2707],zoom_start=5, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

        for i in range(len(loc)):
        # Call the reverse geocoding function with latitude and longitude
            location = geolocator.reverse((loc[i][0],loc[i][1]), exactly_one=True)
        
            if location is not None:
            
                tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Ups-id :"+str(trac[i])
                folium.Marker([loc[i][0],loc[i][1]],tooltip=tooltip).add_to(m)        
        ms=m._repr_html_()
            
    mydict.add("folium",({"map":ms}))
    mydict.add("lt",({"lt":lt}))
    mydict.add("load",({"load":ld}))
    mydict.add("gsm",({"gsm":gsm}))
    mydict.add("astat",({"astat":astat}))
    return render(request, "indexU.html",context=mydict)

def settings_page(request):    
    return render(request, 'settingspage.html')

def Live_map(request):
    ls = ['JIVSJIUPSUPS2517','JIVSJIUPSUPS6387','JIVSJIUPSUPS1097']
    data = []
    m = folium.Map(location=[13.0827,80.2707], width="%100", height="100%",zoom_start=10, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
    for i in range(len(ls)):
        cust_tracker = ls[i]
        g_id = GFRID_UPS.objects.get(tracker=cust_tracker).id
        # ver = GFRID.objects.get(tracker=cust_tracker).version_id
        # ver_code = (Version.objects.get(id=ver).code).lower()
        # sensor_code = "gpsl86"
        # cursor1 = connection.cursor()
        # cursor1.execute(f"SELECT * FROM {ver_code}_{sensor_code} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
        # da = cursor1.fetchall()
        # data.append(da[0])
    # for row in data:
        trac = cust_tracker
        if trac == "JIVSJIUPSUPS2517":
            ups = "UPS-101"
            loc = [13.0067, 80.2206]
        elif trac == "JIVSJIUPSUPS6387":
            ups = "UPS-201"
            loc = [13.0588, 80.2756]
        elif trac == "JIVSJIUPSUPS1097":
            ups = "UPS-301"
            loc = [12.9815, 80.2180]

        # lc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%Y-%m-%d %H:%M:%S")
        # cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
        # cur = datet.datetime.strptime(cur,'%Y-%m-%d %H:%M:%S')
        # lc_ts = datet.datetime.strptime(lc_ts,'%Y-%m-%d %H:%M:%S')
        # loc_ts = timeago.format(lc_ts,cur)
        # lu.append(loc_ts)
        # iframe = folium.IFrame("UPS_Id : "+str(ups))
        try:
            geolocator = Nominatim(user_agent="my_geocoder")
            location = geolocator.reverse((loc[0],loc[1]), exactly_one=True)
            address = location.raw.get("address", {})
            loc_city = address.get('state','')
            loc_pin = address.get('postcode','')
            l_cp = str(loc_city)+"," +str(loc_pin)
            # lua.append(l_cp)
        except GeocoderServiceError as e:
            #print("Geocoder Service Error:", e)
            return None, None
        # popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=70)
        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>UPS ID :"+str(ups)
        folium.Marker(([loc[0],loc[1]]),tooltip=tooltip).add_to(m)
    ms=m._repr_html_()
    mydict = create_dict()
    mydict.add("folium",({"map":ms}))
    return render(request, 'map.html',context=mydict)

def alert_Status(request,upsid):
    if upsid == 'UPS_101':
        trac = 'JIVSJIUPSUPS2517'
    elif(upsid == 'UPS_201'):
        trac = 'JIVSJIUPSUPS6387'
    elif(upsid == 'UPS_301'):
        trac = 'JIVSJIUPSUPS1097'

    g_id = GFRID_UPS.objects.get(tracker=trac).id
    with connections['ups'].cursor() as cursor1:
        cursor1.execute(f"SELECT * FROM ups_ups WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
        da = cursor1.fetchall()

    byte_value = da[0][11]
    alert_colors = []
    stat_alrt = []
    mydict = create_dict()
    if byte_value != None:
        for i in range(len(byte_value[:8])):
            # print(i,byte_value[7])
            if i != 7:
                if int(byte_value[i]) == 1:
                    alert_colors.append("Red")  
                    i_d = da[0][0]
                    print(i_d)
                    # da = 0
                    while da:
                        d_id = i_d-1
                        curs = connection.cursor()
                        curs.execute(f"SELECT * FROM ups_ups WHERE id = {d_id} and gfrid = {g_id}")
                        da = curs.fetchall()
                        # print(da)

                        if not da:
                            # i_d -= 1
                            print('none')

                        # elif da[0][1] == g_id and da[0][11] == None:
                        #     curs1 = connection.cursor()
                        #     curs1.execute(f"SELECT * FROM ups_ups WHERE id = {i_d} and gfrid = {g_id}")
                        #     da1 = curs1.fetchall()
                        #     lt_t1 = datetime.fromtimestamp(da1[0][10]).strftime("%Y-%m-%d %H:%M:%S")
                        #     stat_alrt.append(lt_t1)
                        #     break
                        else:
                            print(da[0][11],i_d)
                            pos = da[0][11][i]
                            # print(da[0][0],pos)
                            if byte_value[i] != da[0][11][i]:
                                cur = connection.cursor()
                                cur.execute(f"SELECT * FROM ups_ups WHERE id = {d_id+1} and gfrid = {g_id}")
                                dd = cur.fetchall()
                                lt_t = datetime.fromtimestamp(dd[0][10]).strftime("%Y-%m-%d %H:%M:%S")
                                stat_alrt.append(lt_t)
                                # print(lt_t)
                                # print(dd[0][10])
                                break
                            i_d -= 1
                else:
                    alert_colors.append("Green")    
                    stat_alrt.append("-")
            else:
                if int(byte_value[i]) == 0:
                    alert_colors.append("Red")  
                    i_d = da[0][0]
                    print(i_d)
                    # da = 0
                    while da[0][11][i] == 1:
                        d_id = i_d-1
                        curs = connection.cursor()
                        curs.execute(f"SELECT * FROM ups_ups WHERE id = {d_id} and gfrid = {g_id}")
                        da = curs.fetchall()
                        # print(da)

                        if not da:
                            # i_d -= 1
                            print('none')
                        else:
                            print(da[0][11],i_d)
                            pos = da[0][11][i]
                            # print(da[0][0],pos)
                            if byte_value[i] != da[0][11][i]:
                                cur = connection.cursor()
                                cur.execute(f"SELECT * FROM ups_ups WHERE id = {d_id+1} and gfrid = {g_id}")
                                dd = cur.fetchall()
                                lt_t = datetime.fromtimestamp(dd[0][10]).strftime("%Y-%m-%d %H:%M:%S")
                                stat_alrt.append(lt_t)
                                # print(lt_t)
                                # print(dd[0][10])
                                break
                            i_d -= 1
                else:
                    alert_colors.append("Green")    
                    stat_alrt.append("-")

            mydict["alert_colors"] = alert_colors
            mydict["stat_alrt"] = stat_alrt
            mydict["ups"] = upsid
    print(mydict)    
    return render(request, "alertStatus.html",context=mydict,)

def alerts_page(request):
    formatted_data = []
    ups = request.POST.get('upsSelect')
    print(ups)
    selected_alerts = request.POST.getlist('alertSelect')
    print(selected_alerts)

    dt = request.POST.get('datetimes') 
    print(dt)  
    if ups == 'UPS-101':
        trac = 'JIVSJIUPSUPS2517'
    elif(ups == 'UPS-201'):
        trac = 'JIVSJIUPSUPS6387'
    elif(ups == 'UPS-301'):
        trac = 'JIVSJIUPSUPS1097'
    
    if ups:
        g_id = GFRID_UPS.objects.get(tracker=trac).id
        print(g_id)
    if not dt:
        print("No selected date")
    elif not selected_alerts:
        print("please select atleast one alert")
    else:
        dates = dt.split("-")
        # print("DATERANGE:: ",dates)
        s = dates[0]
        t =  dates[0].strip()
        start = t.replace("/","-")
        # drs = datetime.strftime(t, '%Y %b %d %H:%M:%S')
        last = datetime.strptime(start, '%Y %b %d').astimezone(pytz.timezone("UTC"))
        start_date = round(last.replace(tzinfo=timezone.utc).timestamp())
        # # print(int(start_date))
        e = dates[1]
        nw = dates[1].strip()
        end = nw.replace("/","-")
        # dre = datetime.strftime(nw, '%Y %b %d %H:%M:%S')
        now = datetime.strptime(end, '%Y %b %d').astimezone(pytz.timezone("UTC"))
        end_date = round(now.replace(tzinfo=timezone.utc).timestamp())
        print(start_date,end_date)

        with connections['ups'].cursor() as cur:
            cur.execute(f'''select ts,{','.join(selected_alerts)} FROM ups_ups WHERE gfrid={g_id} and ts BETWEEN {start_date} AND {end_date}''')
            da = cur.fetchall()
            print(da)
        columns = [desc[0] for desc in cur.description]

        main = []
        for i in range(len(da)):
            main.append([])
        # Now, you can access your data and column names
        for row in range(len(da)):
            for col_name, value in zip(columns, da[row]):
                if col_name == 'ts':
                    main[row].append(datetime.fromtimestamp(value).strftime("%Y-%m-%d %H:%M:%S"))
                else:
                    if value == '0':
                        main[row].append((col_name,'Inactive'))
                    elif(value == '1'):
                        main[row].append((col_name,'Active'))
                # print(f"{col_name}: {value}")
        # print(main)
        
        # Iterate through the original data
        for row in main:
            timestamp = row[0]
            alert_data = row[1:]
            
            for alert in alert_data:
                alert_type, alert_status = alert
                if alert_type == 'utility_no_failure':
                    alerttype = 'Utility Fail'
                elif(alert_type == 'battery_not_low'):
                    alerttype = 'Battery low'
                elif(alert_type == 'bypass_inactive'):
                    alerttype = 'Bypass/Boost or Buck Active'
                elif(alert_type == 'ups_no_failure'):
                    alerttype = 'UPS Failed'
                elif(alert_type == 'ups_online'):
                    alerttype = 'UPS Type is standby'
                elif(alert_type == 'no_test'):
                    alerttype = 'Test in Progress'
                elif(alert_type == 'shutdown_inactive'):
                    alerttype = 'Shutdown Active'
                elif(alert_type == 'beeper_on'):
                    alerttype = 'Beeper On'
                formatted_data.append({
                    "alerttype": alerttype,
                    "datetime": timestamp,
                    "alertstatus": alert_status
                })
    # print(formatted_data)
    return render(request, 'alertspage.html',context={'data':formatted_data,'ups':ups,'alert':selected_alerts,'dt':dt})

def UpsDetail(request,upsid):
    if upsid == 'UPS_101':
        trac = 'JIVSJIUPSUPS2517'
    elif(upsid == 'UPS_201'):
        trac = 'JIVSJIUPSUPS6387'
    elif(upsid == 'UPS_301'):
        trac = 'JIVSJIUPSUPS1097'

    g_id = GFRID_UPS.objects.get(tracker=trac).id
    with connections['ups'].cursor() as cursor_tob:
        cursor_tob.execute(f"SELECT * FROM ups_ups WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
        da = cursor_tob.fetchall()

    byte_value = da[0][11]

    cur_time = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')

    if int(byte_value[0]) == 0:
        time_on_battery = "----"
    else:
        # da = None
        while da:
            i_d = da[0][0]
            d_id = i_d-1
            curs = connection.cursor()
            curs.execute(f"SELECT * FROM ups_ups WHERE id = {d_id} and gfrid = {g_id}")
            da = curs.fetchall()

            if not da:
                # i_d -= 1
                print('none')
            else:
                print(da[0][11],i_d)
                pos = da[0][11][0]
                # print(da[0][0],pos)
                if byte_value[0] != da[0][11][0]:
                    cur = connection.cursor()
                    cur.execute(f"SELECT * FROM ups_ups WHERE id = {d_id+1} and gfrid = {g_id}")
                    dd = cur.fetchall()
                    lt_t = datetime.fromtimestamp(dd[0][10]).strftime("%Y-%m-%d %H:%M:%S")
                    break
                i_d -= 1
        time_on_battery = timeago.format(lt_t,cur_time)

    with connections['ups'].cursor() as cursor1:
        cursor1.execute(f"SELECT * FROM ups_ups WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
        da = cursor1.fetchall()
    o_ld = da[0][6]
    c_ld = round(4.5*o_ld/100,2)
    # print(da[0][3])

    dt_range = request.POST.get('datetimes')
    if not dt_range:
        dt_string = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y %b %d')
        dt_string3 = (datetime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=2)).strftime('%Y %b %d')
        
        dt_range = f"{dt_string3} - {dt_string}"
        dates = dt_range.split("-")
        
        dt_ef = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d')
        dt_sf = (datetime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=1)).strftime('%Y-%m-%d')

        dt_e = datetime.strptime(dt_ef, '%Y-%m-%d').astimezone(pytz.timezone("UTC"))
        dt_s = datetime.strptime(dt_sf, '%Y-%m-%d').astimezone(pytz.timezone("UTC"))

        start_date = round(dt_s.replace(tzinfo=timezone.utc).timestamp())
        end_date = round(dt_e.replace(tzinfo=timezone.utc).timestamp())

        print("1",start_date,end_date)

    else:
        dates = dt_range.split("-")
        # print("DATERANGE:: ",dates)
        s = dates[0]
        t =  dates[0].strip()
        start = t.replace("/","-")
        # drs = datetime.strftime(t, '%Y %b %d %H:%M:%S')
        last = datetime.strptime(start, '%Y %b %d').astimezone(pytz.timezone("UTC"))
        
        start_date = round(last.replace(tzinfo=timezone.utc).timestamp())
        # # print(int(start_date))
        e = dates[1]
        nw = dates[1].strip()
        end = nw.replace("/","-")
        # dre = datetime.strftime(nw, '%Y %b %d %H:%M:%S')
        now = datetime.strptime(end, '%Y %b %d').astimezone(pytz.timezone("UTC"))
        
        end_date = round(now.replace(tzinfo=timezone.utc).timestamp())

    with connections['ups'].cursor() as cursor2: 
        cursor2.execute(f"SELECT * FROM ups_ups WHERE gfrid = {g_id} and ts BETWEEN {start_date} AND {end_date}")
        dat = cursor2.fetchall()
    Ah = 7
    Effi = 0.9
    b_d = da[0][8]*18
    load_ebrt = da[0][6]
    # EBRT calculation 
    if load_ebrt != 0 and da[0][3] == 0:     
        Ebrt_Dat = round((Ah*Effi*b_d)/(load_ebrt*10),2)
        # Convert to hours and mins
        total_minutes = int(Ebrt_Dat * 60)
        hours = total_minutes // 60
        mins = total_minutes % 60

    else:
        hours = '--'
        mins = '--'

    i_v = []
    tm = []
    o_v = []
    i_f = []
    tem = []
    b_v = []
    f_v = []
    cl_ld = []
    ri_f = []
    ri_t = []
    ri_io = []
    for i in dat:
        ri_io.append(230)
        ri_f.append(50)
        ri_t.append(30)
        i_v.append(i[3])
        f_v.append(i[4])
        o_v.append(i[5])
        cl_ld.append(round(4.5*i[6]/100,2))
        i_f.append(i[7])
        b_v.append(i[8])
        tem.append(i[9])
        tm.append(datetime.fromtimestamp(int(i[10]),tz.tzutc()).strftime('%Y %b %d %H:%M:%S'))

    trace1 = go.Scatter(x = tm, y = i_v, mode = 'lines', name= 'IP-Volt' ,line=dict(color="#8B8000"))
    trace2 = go.Scatter(x = tm, y = f_v, mode = 'lines', name= 'IP-Fault-Volt' ,line=dict(color="#FA9F30"))
    trace3 = go.Scatter(x = tm, y = o_v, mode = 'lines', name= 'OP-Volt' ,line=dict(color="#0052cc"))
    trace11 = go.Scatter(x = tm, y = ri_io, mode = 'lines', name= 'RatedInfo-IP/OP-Volt' ,line=dict(color="#2FB540"))
   # trace22 = go.Scatter(x = tm, y = ri_io, mode = 'lines', name= 'RatingInfo-OP-Volt' ,line=dict(color="#2FB540"))
    # trace33 = go.Scatter(x = tm, y = ri_f, mode = 'lines', name= 'RI_OP-Volt' ,line=dict(color="#0052cc"))

    # trace3 = go.Scatter(x = xaxis, y = accz_yaxis, mode = 'lines', name= 'AccZ')
    data = [trace1,trace2,trace3,trace11]
    layout = go.Layout(title="Input/Output voltage",yaxis_title="Volt(V)",margin=go.layout.Margin(l=50, r=50, b=0, t=100), font=dict(size=12),height=300)
    fig1 = go.Figure(data = data, layout = layout)
    fig1.update_layout(legend=dict(orientation='h', yanchor='bottom', y=0.0, xanchor='center', x=0.5))
    fig1.update_xaxes(nticks=10,tickvals=[])
    fig1.update_yaxes(range=[0,270])

    trace4 = go.Scatter(x= tm, y = i_f,mode = 'lines', name='Input-Frequency', line=dict(color="#3898D6"))
    trace44 = go.Scatter(x= tm, y = ri_f,mode = 'lines', name='RatedInfo-Input-Frequency', line=dict(color="#2FB540"))
    data1 = [trace4,trace44]
    layout1 = go.Layout(title="Input Frequency",yaxis_title="Hertz(Hz)",margin=go.layout.Margin(l=50, r=50, b=0, t=80), font=dict(size=12),height=300)
    fig2 = go.Figure(data = data1, layout = layout1)
    fig2.update_layout(legend=dict(orientation='h', yanchor='bottom', y=0.0, xanchor='center', x=0.5))
    fig2.update_xaxes(nticks=10,tickvals=[])
    fig2.update_yaxes(range=[0,51])

    trace5 = go.Scatter(x= tm, y = b_v,mode = 'lines', name='Battery-Volt', line=dict(color="#3898D6"))
    data2 = [trace5]
    layout2 = go.Layout(title="Battery-Voltage",yaxis_title="Volt(V)",margin=go.layout.Margin(l=50, r=50, b=0, t=80), font=dict(size=12),height=300)
    fig3 = go.Figure(data = data2, layout = layout2)
    fig3.update_layout(legend=dict(orientation='h', yanchor='bottom', y=0.0, xanchor='center', x=0.5))
    fig3.update_xaxes(nticks=10,tickvals=[])
    fig3.update_yaxes(range=[0,30])

    trace6 = go.Scatter(x= tm, y = tem,mode = 'lines', name='Temperature', line=dict(color="#3898D6"))
    trace66 = go.Scatter(x= tm, y = ri_t,mode = 'lines', name='RatedInfo-Temperature', line=dict(color="#2FB540"))
    data3 = [trace6,trace66]
    layout3 = go.Layout(title="Temperature",yaxis_title="Celsius(°C)",margin=go.layout.Margin(l=50, r=50, b=0, t=80), font=dict(size=12),height=350,)
    fig4 = go.Figure(data = data3, layout = layout3)
    fig4.update_layout(legend=dict(orientation='h', yanchor='bottom', y=0.0, xanchor='center', x=0.5))
    fig4.update_xaxes(nticks=10,tickvals=[])
    fig4.update_yaxes(range=[0,60])

    trace7 = go.Scatter(x= tm, y = cl_ld,mode = 'lines', name='load', line=dict(color="#3898D6"))
    data4 = [trace7]
    layout4 = go.Layout(title="Load",yaxis_title="percentage(%)",margin=go.layout.Margin(l=50, r=50, b=0, t=80), font=dict(size=12),height=300)
    fig5 = go.Figure(data = data4, layout = layout4)
    fig5.update_layout(legend=dict(orientation='h', yanchor='bottom', y=0.0, xanchor='center', x=0.5))
    fig5.update_xaxes(nticks=10,tickvals=[])
    fig5.update_yaxes(range=[-10,200])

    cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
    g = GSMSQL_UPS.objects.filter(GFRID=g_id).last()
    gsm = timeago.format(datetime.fromtimestamp(g.TS).strftime("%Y-%m-%d %H:%M:%S"),cur)
    lt_t = datetime.fromtimestamp(da[0][10]).strftime("%Y-%m-%d %H:%M:%S")
    lt = timeago.format(lt_t,cur)

    print(upsid)

    return render(request, 'upsdetail.html',context={'ip_op': offline.plot((fig1),output_type='div',config={'displaylogo':False}),'ip_fq': offline.plot((fig2),output_type='div',config={'displaylogo':False}),'ld':o_ld,'cal_load':c_ld,'battery': offline.plot((fig3),output_type='div',config={'displaylogo':False}),'temp': offline.plot((fig4),output_type='div',config={'displaylogo':False}),
    'load': offline.plot((fig5),output_type='div',config={'displaylogo':False}),'v_d':round(da[0][3],2),'f_v_d':round(da[0][4],2),'o_v_d':round(da[0][5],2),'i_f_d':round(da[0][7],2),'b_d':round((da[0][8]*18),2),'t_d':round(da[0][9],2),'upsid':upsid,'dt': f"Date Range Selected : {dt_range}",
    'l_s':lt,'gsm':gsm,'c_ld':round(4.5*da[0][6]/100,2),'tob':time_on_battery,'hours':hours,'mins':mins})

def specification_page(request):    
    return render(request, 'specificationpage.html')

##################################################################################################################

##############################################################Cold#####################################################################################

def Cold(request):
    geolocator = Nominatim(user_agent="my_application")
    loc = [(12.9815,80.2180),(9.9252, 78.1198),(8.0883,77.5385),(15.9129,79.7400)]
    trac = ["TRACCS001","TRACCS002","TRACCS003","TRACCS004"]
    mydict = create_dict()
    m = folium.Map(location=[13.0827,80.2707],zoom_start=5, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)

    for i in range(len(loc)):
    # Call the reverse geocoding function with latitude and longitude
        location = geolocator.reverse((loc[i][0],loc[i][1]), exactly_one=True)
    
        if location is not None:
	      
            tooltip = "<strong>LOCATION :</strong>"+str(location).replace(", ", ", <br>") +"<br><strong>VEHICLE : </strong>"+str(trac[i])
            folium.Marker([loc[i][0],loc[i][1]],tooltip=tooltip).add_to(m)
  
        ms=m._repr_html_()
            
    mydict.add("folium",({"map":ms}))
    return render(request,"indexC.html",context=mydict) 

def SensorD(request,tracker):
    mydict = create_dict()
    
    fig = go.Figure()

    if tracker=="TRACCS001":
        temp = [-4.0,-3.5,3.0,3.1]
        fig.add_trace(go.Indicator(
            mode = "gauge",
            value=-4.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f = go.Figure(fig)
        mydict.add("plot1",({"plot1":offline.plot((f),output_type='div',config= dict(
                displayModeBar = False))}))

        fig1 = go.Figure()

        fig1.add_trace(go.Indicator(
            mode = "gauge",
            value=-3.5,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig1.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f1 = go.Figure(fig1)
        mydict.add("plot2",({"plot2":offline.plot((f1),output_type='div',config= dict(
                displayModeBar = False))}))

        fig2 = go.Figure()

        fig2.add_trace(go.Indicator(
            mode = "gauge",
            value=3.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig2.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f2 = go.Figure(fig2)
        mydict.add("plot3",({"plot3":offline.plot((f2),output_type='div',config= dict(
                displayModeBar = False))}))

        fig3 = go.Figure()
        fig3.add_trace(go.Indicator(
            mode = "gauge",
            value=3.1,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}
                    ))
        fig3.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f3 = go.Figure(fig3)
        mydict.add("plot4",({"plot4":offline.plot((f3),output_type='div',config= dict(
                displayModeBar = False))}))
        
        mydict.add("temp",({"temp":temp,"tracker":tracker}))

        return render(request,"cold.html",context=mydict)

    elif tracker=="TRACCS002":
        temp = [3.0,-1.5,-7.0,2.1]
        fig.add_trace(go.Indicator(
            mode = "gauge",
            value=3.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f = go.Figure(fig)
        mydict.add("plot1",({"plot1":offline.plot((f),output_type='div',config= dict(
                displayModeBar = False))}))

        fig1 = go.Figure()

        fig1.add_trace(go.Indicator(
            mode = "gauge",
            value=-1.5,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig1.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f1 = go.Figure(fig1)
        mydict.add("plot2",({"plot2":offline.plot((f1),output_type='div',config= dict(
                displayModeBar = False))}))

        fig2 = go.Figure()

        fig2.add_trace(go.Indicator(
            mode = "gauge",
            value=-7.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig2.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f2 = go.Figure(fig2)
        mydict.add("plot3",({"plot3":offline.plot((f2),output_type='div',config= dict(
                displayModeBar = False))}))

        fig3 = go.Figure()
        fig3.add_trace(go.Indicator(
            mode = "gauge",
            value=2.1,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}
                    ))
        fig3.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f3 = go.Figure(fig3)
        mydict.add("plot4",({"plot4":offline.plot((f3),output_type='div',config= dict(
                displayModeBar = False))}))
        
        mydict.add("temp",({"temp":temp,"tracker":tracker}))

        return render(request,"cold.html",context=mydict)
    
    elif tracker=="TRACCS003":
        temp = [6.0,-5.5,1.0,-6.1]
        fig.add_trace(go.Indicator(
            mode = "gauge",
            value=6.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f = go.Figure(fig)
        mydict.add("plot1",({"plot1":offline.plot((f),output_type='div',config= dict(
                displayModeBar = False))}))

        fig1 = go.Figure()

        fig1.add_trace(go.Indicator(
            mode = "gauge",
            value=-5.5,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig1.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f1 = go.Figure(fig1)
        mydict.add("plot2",({"plot2":offline.plot((f1),output_type='div',config= dict(
                displayModeBar = False))}))

        fig2 = go.Figure()

        fig2.add_trace(go.Indicator(
            mode = "gauge",
            value=1.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig2.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f2 = go.Figure(fig2)
        mydict.add("plot3",({"plot3":offline.plot((f2),output_type='div',config= dict(
                displayModeBar = False))}))

        fig3 = go.Figure()
        fig3.add_trace(go.Indicator(
            mode = "gauge",
            value=-6.1,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}
                    ))
        fig3.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f3 = go.Figure(fig3)
        mydict.add("plot4",({"plot4":offline.plot((f3),output_type='div',config= dict(
                displayModeBar = False))}))
        
        mydict.add("temp",({"temp":temp,"tracker":tracker}))

        return render(request,"cold.html",context=mydict)
    
    elif tracker=="TRACCS004":
        temp = [-19.0,3.5,-3.0,4.1]
        fig.add_trace(go.Indicator(
            mode = "gauge",
            value=-19.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f = go.Figure(fig)
        mydict.add("plot1",({"plot1":offline.plot((f),output_type='div',config= dict(
                displayModeBar = False))}))

        fig1 = go.Figure()

        fig1.add_trace(go.Indicator(
            mode = "gauge",
            value=3.5,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig1.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f1 = go.Figure(fig1)
        mydict.add("plot2",({"plot2":offline.plot((f1),output_type='div',config= dict(
                displayModeBar = False))}))

        fig2 = go.Figure()

        fig2.add_trace(go.Indicator(
            mode = "gauge",
            value=-3.0,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}))

        fig2.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f2 = go.Figure(fig2)
        mydict.add("plot3",({"plot3":offline.plot((f2),output_type='div',config= dict(
                displayModeBar = False))}))

        fig3 = go.Figure()
        fig3.add_trace(go.Indicator(
            mode = "gauge",
            value=4.1,
            # delta = {'reference': 200},
            domain = {'x': [0.25,0.25], 'y': [0.4, 0.6]},

            gauge = {
                'shape': "bullet",
                'axis': {'range': [-20,8]},
                'threshold': {
                    'line': {'color': "rgb(165, 42, 42)", 'width': 3},
                    'thickness': 1,
                    'value': -6},
                'steps': [
                    {'range': [-20, -10], 'color': "rgb(52,170,255)"},
                    {'range': [-10, -2], 'color': "rgb(146,208,80)"},
                    {'range': [-2, 8], 'color': "rgb(255,192,0)"}],
                'bar': {'color': "black"}}
                    ))
        fig3.update_layout(height=70,width=140,margin=go.layout.Margin(l=0, r=0, b=0, t=0), font=dict(size=10))
        f3 = go.Figure(fig3)
        mydict.add("plot4",({"plot4":offline.plot((f3),output_type='div',config= dict(
                displayModeBar = False))}))
        
        mydict.add("temp",({"temp":temp,"tracker":tracker}))

        return render(request,"cold.html",context=mydict)

#######################################################################################################################################################

#############################################HEAVY MACHINERY################################################################

def Mac(request):
    data = []
    lu = []
    lua = []
    l_on = []
    l_com = []
    bn = []
    o_f = []
    f_f = []
    last_7 = []
    last_ts = []
    t_of = []
    t_of_m = []
    m_t = []
    
    # Get today's date
    end_date = datetime.now() - timedelta(days=1)

    # Generate a list of dates for the previous 7 days
    previous_7_days = [end_date - timedelta(days=i) for i in range(7)]
    previous_7_days = previous_7_days[::-1]
    #print(previous_7_days)

    # #print the dates
    for date in previous_7_days:
        last_7.append(date.strftime('%-d %b'))
        # last_7 = last_7[::-1]
        f = date.strftime('%Y %m %d')
        l = datet.datetime.strptime(f,'%Y %m %d')
        # # #print(f)
        last_ts.append(round(datet.datetime.timestamp(l)))
        # last_ts= last_ts[::-1]
    # #print(last_ts)

    c_id ="clinohealth67"
    c_pass ="onilc@112022"
    # ls = ['JIVSJILDVLCS3086','JIVSJILDVLCS8842','JIVSJILDVLCS3703','JIVSJILDVLCS5221']
    # ,'JIVSJIVERVME7124'
    ls = ['JIVSJIVERVME3693','JIVSJIVERVME9140','JIVSJIVERVME7124']
    mydict = create_dict()

    for e in range(len(ls)):
        m_t.append([])
    
    for i in range(len(ls)):
        for j in range(7):
            m_t[i].append([])
    
    for t in range(len(ls)):                
        cust_tracker = ls[t]
    
        global subs_start
        global subs_end
        
        subs_start = 1669899600
        subs_end = 1709040092

        exst = GFRID_UPS.objects.filter(tracker=cust_tracker).exists()
        g_id = GFRID_UPS.objects.get(tracker=cust_tracker).id

        bn.append(TrackerCommands_ver.objects.get(tracker=cust_tracker,commandID=5).process_flag)
        o_f.append(TrackerCommands_ver.objects.get(tracker=cust_tracker,commandID=5).tester_flag)
        f_f.append(TrackerCommands_ver.objects.get(tracker=cust_tracker,commandID=6).tester_flag)

        last_on = Alerts.objects.filter(alert="0x00000800",GFRID=g_id).last()
        last_com = GSMSQL_UPS.objects.filter(GFRID=g_id).last()
        cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')

        r = 0
        o = 0
        while r<=6:
            for i in range(len(last_ts)):
                s_date = last_ts[i]
                e_date = last_ts[i] + 86399
                #print(s_date,e_date)
                t_o = Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt__range=[s_date,e_date])
                
                for n in t_o:
                    m_t[t][i].append(n.TS_BigInt)
                    dt_ef = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d %H:%M:%S')                
                    dt_e = datetime.strptime(dt_ef, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))

                    if Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']:
                        of_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                        on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                        if on_z != of_z:
                            rr_t = datetime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                            # #print("ttttt",rr_t)
                            rr_p = pd.to_datetime(datetime.strftime(datet.datetime.now(),'%Y %m %d'))
                            num_days = (rr_p - rr_t).days
                            print("num111111",num_days)

                            s = i
                            for a in range(num_days):                              
                                if s != i:
                                    # print(of_z,datetime.fromtimestamp(last_ts[s]).strftime("%Y %m %d") + " 00:00:00")
                                    if of_z == datetime.fromtimestamp(last_ts[s]).strftime("%Y %m %d") + " 00:00:00":
                                        break                        
                                    elif(last_ts[s] not in m_t[t][s]): 
                                        m_t[t][s].append(last_ts[s])
                                    r_tt = datetime.fromtimestamp(m_t[t][s][-1]).strftime("%Y %m %d") + " 23:59:59"
                                    r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                                    if round(datet.datetime.timestamp(r_pp)) not in m_t[t][s]:
                                        m_t[t][s].append(round(datet.datetime.timestamp(r_pp))) 
                                s = s+1

                        else:
                            rr_t = datetime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                            # #print("ttttt",rr_t)
                            rr_p = pd.to_datetime(datetime.strftime(datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']),'%Y %m %d'))
                            num_days = (rr_p - rr_t).days
                            # print("num111111",num_days)

                            for i in range(num_days):
                                # #print(i,ts_list[i])
                                if last_ts[i] not in m_t[t][i]:
                                    m_t[t][i].append(last_ts[i])
                                r_tt = datetime.fromtimestamp(m_t[t][i][-1]).strftime("%Y %m %d") + " 23:59:59"
                                r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                                if round(datet.datetime.timestamp(r_pp)) not in m_t[t][i]:
                                    m_t[t][i].append(round(datet.datetime.timestamp(r_pp)))
                                current_date = rr_t + timedelta(days=i)
                                day = current_date.day
                                month = current_date.strftime('%m')
                                year = current_date.year
                                # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
                    else:
                        #print("currrr",datet.datetime.now().timestamp())
                        m_t[t][i].append(e_date)
                        on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d")
                        rr_t = datetime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                        # #print("ttttt",rr_t)
                        rr_p = datetime.strptime(datetime.fromtimestamp(last_ts[6]).strftime("%Y %m %d"),'%Y %m %d')
                        num_days = (rr_p - rr_t).days
                        # #print("num111111",num_days)
                        s = i
                        for a in range(num_days): 
                            # if s != i:
                            s = s +1
                            print(s,len(last_ts))
                            if s <= len(last_ts)-1:
                                if on_z == last_ts[s]:
                                    break                                              
                                elif(last_ts[s] not in m_t[t][s]): 
                                    m_t[t][s].append(last_ts[s])
                                # r_tt = datetime.fromtimestamp(r_p,tz=timezone.utc)
                                # r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                                if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[t][s]:
                                    m_t[t][s].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp())) 
                            else:
                                m_t[t][i].append(last_ts[i])
                                if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[t][i]:
                                    m_t[t][i].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp()))

                t_f = Alerts.objects.filter(GFRID=g_id,alertNotify_id=9,TS_OFF_BigInt__range=[s_date,e_date])   

                # #print("t_f",t_f)
                for m in t_f:
                    # #print(m.TS_BigInt)
                    rr_t = datetime.strptime(datetime.fromtimestamp(m.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                    rr_p = datetime.strptime(datetime.fromtimestamp(m.TS_OFF_BigInt).strftime("%Y %m %d"),'%Y %m %d')

                    num_days = (rr_p - rr_t).days
                    # #print("num",num_days)
                    for j in range(num_days + 1):
                        current_date = rr_t + timedelta(days=j)
                        day = current_date.day
                        month = current_date.strftime('%m')
                        year = current_date.year
                        # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
                            
                        if m.TS_OFF_BigInt not in m_t[t][i]:
                            # #print("ooooofff",i,m.TS_OFF_BigInt)
                            m_t[t][i].append(m.TS_OFF_BigInt)
                r = r+1  

        if last_on:
            if last_on.TS_OFF_BigInt:
                lt_t = datetime.fromtimestamp(last_on.TS_OFF_BigInt).strftime("%Y-%m-%d %H:%M:%S")
                lt = timeago.format(lt_t,cur)
                l_on.append(lt)
            else:
                lt_t = datetime.fromtimestamp(last_on.TS_BigInt).strftime("%Y-%m-%d %H:%M:%S")
                lt = timeago.format(lt_t,cur)
                l_on.append("Running")
        else:
            l_on.append("no status")

        if last_com:
            lt_c = datetime.fromtimestamp(last_com.TS).strftime("%Y-%m-%d %H:%M:%S")
            lc = timeago.format(lt_c,cur)
            l_com.append(lc)
        else:
            l_com.append("no status")

        if exst is True:
            ts_now = round(datetime.now().replace(tzinfo=timezone.utc).timestamp())
            if ts_now < subs_end:
                ver = GFRID_UPS.objects.get(tracker=cust_tracker).version_id
                ver_code = (Version_UPS.objects.get(id=ver).code).lower()
                sensor_code = "gpsl86"
                with connections['ups'].cursor() as cursor2:
                    cursor2.execute(f"SELECT * FROM {ver_code}_{sensor_code} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
                    da = cursor2.fetchall()
                    print(da)
                data.append(da[0])    
                mydict.add("Customer",({"name": f"{c_id}","tracker":cust_tracker,"l_on":l_on,"bn":bn,"o_f":o_f,"f_f":f_f,'l_com':l_com}))
        #print(o_f,f_f,bn)
        
        a_id = Alerts.objects.filter(GFRID=g_id,alertNotify_id=9)
        for t in range(len(ls)):            
            for i in range(len(m_t[t])):                    
                if len(m_t[t][i]) == 0:
                    print("empty")

                elif(sorted(m_t[t][i])[0] in [i.TS_OFF_BigInt for i in a_id]) and (sorted(m_t[t][i])[-1] in [i.TS_BigInt for i in a_id]):                
                    r_t = datetime.fromtimestamp(m_t[t][i][0]).strftime("%Y %m %d") + " 00:00:00"
                    r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                    if round(datet.datetime.timestamp(r_p)) not in m_t[t][i]:
                        m_t[t][i].append(round(datet.datetime.timestamp(r_p)))

                    r_tt = datetime.fromtimestamp(m_t[t][i][-1]).strftime("%Y %m %d") + " 23:59:59"
                    r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                    if round(datet.datetime.timestamp(r_pp)) not in m_t[t][i]:
                        m_t[t][i].append(round(datet.datetime.timestamp(r_pp)))

                elif(sorted(m_t[t][i])[0] in [i.TS_OFF_BigInt for i in a_id]):
                    # #print(m_t[t][i][0])
                    r_t = datetime.fromtimestamp(m_t[t][i][0]).strftime("%Y %m %d") + " 00:00:00"
                    r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                    if round(datet.datetime.timestamp(r_p)) not in m_t[t][i]:
                        m_t[t][i].append(round(datet.datetime.timestamp(r_p)))

                elif(sorted(m_t[t][i])[-1] in [i.TS_BigInt for i in a_id]):
                    print(m_t[t][i][-1],[i.TS_BigInt for i in a_id])                
                    r_t = datetime.fromtimestamp(m_t[t][i][-1]).strftime("%Y %m %d") + " 23:59:59"
                    r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                    if round(datet.datetime.timestamp(r_p)) not in m_t[t][i]:
                        m_t[t][i].append(round(datet.datetime.timestamp(r_p)))
                    
                elif(m_t[t][i][0] in last_ts):                
                    r_t = datetime.fromtimestamp(m_t[t][i][0]).strftime("%Y %m %d") + " 23:59:59"
                    r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
                    if round(datet.datetime.timestamp(r_p)) not in m_t[t][i]:
                        m_t[t][i].append(round(datet.datetime.timestamp(r_p)))

    main_ts_list = []
    # main_ts_l = []
    for i in range(len(ls)):
        main_ts_list.append([])
        # main_ts_l.append([])
        for j in range(len(m_t[i])):
            main_ts_list[i].append([])
            
    for i in range(len(ls)):
        for j in range(len(m_t[i])):
            main_ts_list[i][j].extend(sorted(set((m_t[i][j]))))
    
    print(main_ts_list)

    m_list = []
    for i in range(len(ls)):
        m_list.append([])
        for j in range(len(main_ts_list[i])):
            m_list[i].append([])

    for i in range(len(ls)):
        for j in range(len(main_ts_list[i])):
            timestamp = main_ts_list[i][j]
            datetime_objects = [datetime.fromtimestamp(timestamp) for timestamp in timestamp]

            # Calculate time differences in hours
            time_differences_hours = []
            for k in range(1, len(datetime_objects)):
                time_diff = (datetime_objects[k] - datetime_objects[k - 1]).total_seconds() / 3600
                time_differences_hours.append(time_diff)

            # #print the hour-wise time differences
            for k, diff in enumerate(time_differences_hours, start=1):
                m_list[i][j].append(float(f"{diff:.2f}"))
                # #print(f"Hour {k}: {diff:.2f} hours")
    # #print(m_list)

    mn_list = []
    for i in range(len(ls)):
        mn_list.append([])
        for j in range(len(m_list[i])):
            mn_list[i].append([])

    for i in range(len(ls)):
        for j in range(len(m_list[i])):
            mn_list[i][j].append(sum(m_list[i][j][::2]))
    # #print(mn_list)

    main_list = []
    total_list = []
    for i in range(len(ls)):
        main_list.append([])

    for i in range(len(mn_list)):
        for j in range(len(mn_list[i])):
            main_list[i].append(round(mn_list[i][j][0]))
        total_list.append(sum(main_list[i]))
    # #print(main_list)

    # print(main_list)
    # print(last_7[::-1],main_list[0][::-1])

    colors1 = ['blue',] * 7
    for j in range(len(main_list[0])):
        if main_list[0][j] > 8:                
            colors1[j] = 'red'
            print(colors1[j])
    colors1 = colors1[::-1]

    trace = {
                "x": last_7[::-1],
                "y": main_list[0][::-1],
                # "line": {"shape": 'hv'},
                # "mode": 'lines',
                "name": 'ASSET_ON_STATUS',
                "type": 'bar',
                "marker_color" : colors1
                }
    data0 = [trace]
    layout = go.Layout(height=50,width=70,margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
    figure = go.Figure(data = data0, layout = layout) 
    figure.update_xaxes(showticklabels=False)
    figure.update_yaxes(showticklabels=False,range = [0,24])
    figure.update_layout(
        yaxis_title="Hours",
        yaxis = dict(
            # tickmode = 'linear',
            tick0 = 2,
            dtick = 2
        )
    )
    plot_div = opy.plot(figure, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False})
    mydict.add("bar",({"plot":plot_div}))

    colors2 = ['blue',] * 7
    for j in range(len(main_list[1])):
        if main_list[1][j] > 8:
            colors2[j] = 'red'
    colors2 = colors2[::-1]

    trace1 = {
                    "x": last_7[::-1],
                    "y": main_list[1][::-1],
                    # "line": {"shape": 'hv'},
                    # "mode": 'lines',
                    "name": 'ASSET_ON_STATUS',
                    "type": 'bar',
                    "marker_color" : colors2
                    }
    data1 = [trace1]
    layout1 = go.Layout(height=50,width=70,margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
    figure1 = go.Figure(data = data1, layout = layout1)
    figure1.update_xaxes(showticklabels=False)
    figure1.update_yaxes(showticklabels=False,range = [0,24])
    figure1.update_layout(
        yaxis_title="Hours",
        yaxis = dict(
            # tickmode = 'linear',
            tick0 = 2,
            dtick = 2
        )
    )           
    plot_div = opy.plot(figure1, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False})
    mydict.add("bar1",({"plot1":plot_div}))

    colors3 = ['blue',] * 7
    for j in range(len(main_list[2])):
        if main_list[2][j] > 8:
            colors3[j] = 'red'
    colors3 = colors3[::-1]

    trace2 = {
                    "x": last_7[::-1],
                    "y": main_list[2][::-1],
                    # "line": {"shape": 'hv'},
                    # "mode": 'lines',
                    "name": 'ASSET_ON_STATUS',
                    "type": 'bar',     
                    "marker_color" : colors3                 
                    }
    data2 = [trace2]
    layout2 = go.Layout(height=50,width=70,margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
    figure2 = go.Figure(data = data2, layout = layout2)
    figure2.update_xaxes(showticklabels=False)
    figure2.update_yaxes(showticklabels=False,range = [0,24])
    figure2.update_layout(
        yaxis_title="Hours",
        yaxis = dict(
            # tickmode = 'linear',
            tick0 = 2,
            dtick = 2
        )
    )
    plot_div = opy.plot(figure2, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False})
    mydict.add("bar2",({"plot2":plot_div}))   

    # colors4 = ['blue',] * 7
    # # colors4[0] = 'red'
    # # colors4[1] = 'red'
    # # colors4[2] = 'red'
    # # colors4[4] = 'red'
    # # colors4[j] = 'red'

    # for j in range(len(main_list[3])):
    #     if main_list[3][j] > 8:
    #         colors4[j] = 'red'
    # colors4 = colors4[::-1]


    # trace3 = {
    #                 "x": last_7[::-1],
    #                 "y":main_list[3][::-1],
    #                 # "y": [24,24,24,7,9,6,0],
    #                 # "line": {"shape": 'hv'},
    #                 # "mode": 'lines',
    #                 "name": 'ASSET_ON_STATUS',
    #                 "type": 'bar',     
    #                 "marker_color" : colors4                 
    #                 }
    # data3 = [trace3]
    # layout3 = go.Layout(height=50,width=70,margin=go.layout.Margin(l=13, r=0, b=0, t=0), font=dict(size=10))
    # figure3 = go.Figure(data = data3, layout = layout3)
    # figure3.update_xaxes(showticklabels=False)
    # figure3.update_yaxes(showticklabels=False,range = [0,24])
    # figure3.update_layout(
    #     yaxis_title="Hours",
    #     yaxis = dict(
    #         # tickmode = 'linear',
    #         tick0 = 2,
    #         dtick = 2
    #     )
    # )
    # plot_div = opy.plot(figure3, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False,'displayModeBar':False})
    # mydict.add("bar3",({"plot3":plot_div}))

    m = folium.Map(location=[13.0827,80.2707], width="%100", height="100%",zoom_start=10, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
    for row in data:
        trac = GFRID_UPS.objects.get(id=row[1]).tracker
        if trac == "JIVSJIVERVME3693":
            cust_trac = 'JIVMOT0001B0001'
            veh = "BL0183"
        elif trac == "JIVSJIVERVME9140":
            cust_trac = 'JIVMOT0002B0002'
            veh = "SC1823"
        elif trac == "JIVSJIVERVME7124":
            cust_trac = 'JIVMOT0003B0003'
            veh = "PT1945"
        # elif trac == "JIVSJILDVLCS6123":
        #     cust_trac = 'JIVMOT0004B0004'
        #     veh = "MS0123"

        lc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%Y-%m-%d %H:%M:%S")
        cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
        cur = datet.datetime.strptime(cur,'%Y-%m-%d %H:%M:%S')
        lc_ts = datet.datetime.strptime(lc_ts,'%Y-%m-%d %H:%M:%S')
        loc_ts = timeago.format(lc_ts,cur)
        lu.append(loc_ts)
        iframe = folium.IFrame("Lat : "+str(cust_trac))
        try:
            geolocator = Nominatim(user_agent="my_geocoder")
            location = geolocator.reverse((row[3].strip()[:-1],row[4].strip()[:-1]), exactly_one=True)
            address = location.raw.get("address", {})
            loc_city = address.get('state','')
            loc_pin = address.get('postcode','')
            l_cp = str(loc_city)+"," +str(loc_pin)
            lua.append(l_cp)
        except GeocoderServiceError as e:
            #print("Geocoder Service Error:", e)
            return None, None
        # popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=70)
        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Vehicle :"+str(veh)
        folium.Marker(([row[3].strip()[:-1],row[4].strip()[:-1]]),tooltip=tooltip).add_to(m)

    mydict.add("lu",({"TS":lu,"tracker":ls}))    
    mydict.add("lua",({"loc":lua}))
    mydict.add("total",({"total":total_list}))

    return render(request, 'indexM.html',context=mydict)

def ViewLiveM(request,tracker):
    c1 = []
    x_ts = []
    sm_d = []
    sm_e = []
    j = 1

    if tracker == "JIVMOT0001B0001":
        cust_trac = 'JIVSJIVERVME3693'
        veh = "BL0183"
        ty = "Balers"
    elif tracker == "JIVMOT0002B0002":
        cust_trac = 'JIVSJIVERVME9140'
        veh = "SC1823"
        ty = "Stump Cutters"
    elif tracker == "JIVMOT0003B0003":
        cust_trac = 'JIVSJIVERVME7124'
        veh = "PT1945"
        ty = "Pipeline Trenchers"
    # elif tracker == "JIVMOT0004B0004":
    #     cust_trac = 'JIVSJILDVLCS6123'
    #     veh = "MS0123"
    #     ty = "Mini Skid Steers"
    
    sensorname = 'gpsl86'

    ver =  GFRID_UPS.objects.get(tracker=cust_trac).version_id
    verCode = (Version_UPS.objects.get(id=ver).code).lower()

    tblename = verCode.lower()+"_"+sensorname.lower()

    Gfr = GFRID_UPS.objects.get(tracker = cust_trac)
    gfrid = Gfr.id
    dt_range = request.POST.get('datetimes')

    last_on = Alerts.objects.filter(alert="0x00000800",GFRID=gfrid).last()
    last_com = GSMSQL_UPS.objects.filter(GFRID=gfrid).last()
    cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')

    if last_on:
        if last_on.TS_OFF_BigInt:
            lt_t = datetime.fromtimestamp(last_on.TS_OFF_BigInt).strftime("%Y-%m-%d %H:%M:%S")
            lt = timeago.format(lt_t,cur)
            l_on=lt
        else:
            lt_t = datetime.fromtimestamp(last_on.TS_BigInt).strftime("%Y-%m-%d %H:%M:%S")
            lt = timeago.format(lt_t,cur)
            l_on="running"
    else:
        l_on="no status"

    if last_com:
        lt_c = datetime.fromtimestamp(last_com.TS).strftime("%Y-%m-%d %H:%M:%S")
        lc = timeago.format(lt_c,cur)
        l_com=lc
    else:
        l_com = "no status"

    with connections['ups'].cursor() as cursor1:
        cursor1.execute(f"SELECT * FROM {verCode}_{sensorname} WHERE gfrid = {gfrid} ORDER BY last_modified DESC LIMIT 1")
        da = cursor1.fetchall()[0]
    # data.append(da[0])

    try:
        geolocator = Nominatim(user_agent="my_geocoder")
        location = geolocator.reverse((da[3].strip()[:-1],da[4].strip()[:-1]), exactly_one=True)
        address = location.raw.get("address", {})
        loc_city = address.get('state','')
        loc_pin = address.get('postcode','')
        l_cp = str(loc_city)+"," +str(loc_pin)

    except GeocoderServiceError as e:
        #print("Geocoder Service Error:", e)
        return None, None

    print(l_on,l_cp)

    if not dt_range:
        dt_string = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y %b %d')
        dt_string3 = (datetime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=7)).strftime('%Y %b %d')
        
        dt_range = f"{dt_string3} - {dt_string}"
        dates = dt_range.split("-")
        
        dt_ef = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d')
        dt_sf = (datetime.now(pytz.timezone("Asia/Calcutta")) - timedelta(days=7)).strftime('%Y-%m-%d')

        dt_e = datetime.strptime(dt_ef, '%Y-%m-%d').astimezone(pytz.timezone("UTC"))
        dt_s = datetime.strptime(dt_sf, '%Y-%m-%d').astimezone(pytz.timezone("UTC"))

        start_date = round(dt_s.replace(tzinfo=timezone.utc).timestamp())
        end_date = round(dt_e.replace(tzinfo=timezone.utc).timestamp())

        # #print("1",start_date,end_date)

    else:
        dates = dt_range.split("-")
        # #print("DATERANGE:: ",dates)
        
        s = dates[0]
        t = dates[0].strip()

        start = t.replace("/","-")
        # drs = datetime.strftime(t, '%Y %b %d %H:%M:%S')
        last = datetime.strptime(start, '%Y %b %d').astimezone(pytz.timezone("UTC"))
        
        start_date = round(last.replace(tzinfo=timezone.utc).timestamp())
        # # #print(int(start_date))
        e = dates[1]
        nw = dates[1].strip()
        end = nw.replace("/","-")
        # dre = datetime.strftime(nw, '%Y %b %d %H:%M:%S')
        now = datetime.strptime(end, '%Y %b %d').astimezone(pytz.timezone("UTC"))
        
        end_date = round(now.replace(tzinfo=timezone.utc).timestamp())

        # #print("2",start_date,end_date)

    current_day = datetime.strptime(datetime.fromtimestamp(start_date).strftime("%Y-%m-%d"), "%Y-%m-%d")
    day_list = []
    while current_day <= datetime.strptime(datetime.fromtimestamp(end_date).strftime("%Y-%m-%d"), "%Y-%m-%d"):
        day_list.append(current_day.strftime("%Y-%m-%d"))            
        current_day += timedelta(days=1)

    ts_list = []
    for i in day_list:
        ts_list.append(round(pd.to_datetime(i).timestamp()))
    
    # #print(type(current_day))
    # #print(datetime.strftime(pd.to_datetime(datetime.fromtimestamp(end_date).strftime("%Y-%m-%d")),"%Y-%m-%d"))
    #print(day_list)
    # #print(ts_list)

    m_t = []
    for e in range(len(ts_list)):
        m_t.append([])

    for i in range(len(ts_list)):
        s_date = ts_list[i]
        e_date = ts_list[i] + 86399
        #print(s_date,e_date)
        t_o = Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt__range=[s_date,e_date])
        
        for n in t_o:
            m_t[i].append(n.TS_BigInt)
            dt_ef = datetime.now(pytz.timezone("Asia/Calcutta")).strftime('%Y-%m-%d %H:%M:%S')                
            dt_e = datetime.strptime(dt_ef, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone("UTC"))

            if Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']:
                of_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d") + " 00:00:00"
                if on_z != of_z:   
                    rr_t = datetime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                    # #print("ttttt",rr_t)
                    rr_p = pd.to_datetime(datetime.strftime(datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']),'%Y %m %d'))
                    num_days = (rr_p - rr_t).days
                    # #print("num111111",num_days)

                    r = i
                    for a in range(num_days):  
                        #print("lllllllllll",r,len(ts_list))
                        if r != i:
                            if r == len(ts_list):
                                break                        
                            elif(ts_list[r] not in m_t[r]): 
                                m_t[r].append(ts_list[r])
                            r_tt = datetime.fromtimestamp(m_t[r][-1]).strftime("%Y %m %d") + " 23:59:59"
                            r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                            if round(datet.datetime.timestamp(r_pp)) not in m_t[r]:
                                m_t[r].append(round(datet.datetime.timestamp(r_pp))) 
                        r = r+1

                else:
                    rr_t = datetime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                    # #print("ttttt",rr_t)
                    rr_p = pd.to_datetime(datetime.strftime(datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_OFF_BigInt')[0]['TS_OFF_BigInt']),'%Y %m %d'))
                    num_days = (rr_p - rr_t).days
                    # #print("num111111",num_days)

                    for i in range(num_days):
                        print(i,ts_list[i])
                        if ts_list[i] not in m_t[i]:
                            m_t[i].append(ts_list[i])
                        r_tt = datetime.fromtimestamp(m_t[i][-1]).strftime("%Y %m %d") + " 23:59:59"
                        r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                        if round(datet.datetime.timestamp(r_pp)) not in m_t[i]:
                            m_t[i].append(round(datet.datetime.timestamp(r_pp)))
                        current_date = rr_t + timedelta(days=i)
                        day = current_date.day
                        month = current_date.strftime('%m')
                        year = current_date.year
                        # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
            else:
                # m_t[i].append(e_date)
                on_z = datetime.fromtimestamp(Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_BigInt=n.TS_BigInt).values('TS_BigInt')[0]['TS_BigInt']).strftime("%Y %m %d")
                rr_t = datetime.strptime(datetime.fromtimestamp(n.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
                # #print("ttttt",rr_t)
                rr_p = pd.to_datetime(datetime.strftime(datet.datetime.now(),'%Y %m %d'))
                # r_p = datet.datetime.now(timezone.utc)
                print(dt_e)
                num_days = (rr_p - rr_t).days
                print("num111111",num_days)
                s = i
                for a in range(num_days): 
                    # if s != i:
                    s = s +1

                    if s <= len(ts_list)-1:
                        if on_z == ts_list[s]:
                            break                                              
                        elif(ts_list[s] not in m_t[s]): 
                            m_t[s].append(ts_list[s])
                        # r_tt = datetime.fromtimestamp(r_p,tz=timezone.utc)
                        # r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
                        if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[s]:
                            m_t[s].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp())) 
                    else:
                        m_t[i].append(ts_list[s-1])
                        if round(dt_e.replace(tzinfo=timezone.utc).timestamp()) not in m_t[s-1]:
                            m_t[i].append(round(dt_e.replace(tzinfo=timezone.utc).timestamp()))
                    # s = s+1

        t_f = Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9,TS_OFF_BigInt__range=[s_date,e_date])   
    
        # #print("t_f",t_f)
        for m in t_f:
            # #print(m.TS_BigInt)
            rr_t = datetime.strptime(datetime.fromtimestamp(m.TS_BigInt).strftime("%Y %m %d"),'%Y %m %d')
            rr_p = datetime.strptime(datetime.fromtimestamp(m.TS_OFF_BigInt).strftime("%Y %m %d"),'%Y %m %d')

            num_days = (rr_p - rr_t).days
            # #print("num",num_days)
            for j in range(num_days + 1):
                current_date = rr_t + timedelta(days=j)
                day = current_date.day
                month = current_date.strftime('%m')
                year = current_date.year
                # #print(rr_t,rr_p,f"{year}-{month}-{day} 00:00:00")
                    
                if m.TS_OFF_BigInt not in m_t[i]:
                    # #print("ooooofff",i,m.TS_OFF_BigInt)
                    m_t[i].append(m.TS_OFF_BigInt)
            
    print(m_t)
    m_tt = []
    for i in range(len(ts_list)):
        m_tt.append([])
    for i in range(len(m_t)):
        if len(m_t[i])!=0:
            m_tt[i].extend(sorted(m_t[i]))
        else:
            m_tt[i].extend(m_t[i])

    a_id = Alerts.objects.filter(GFRID=gfrid,alertNotify_id=9)
    for i in range(len(m_tt)):
        if len(m_tt[i]) == 0:
            print("empty")

        elif(sorted(m_tt[i])[0] in [i.TS_OFF_BigInt for i in a_id]) and (sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]) and Alerts.objects.get(GFRID=gfrid,alertNotify_id=9,TS_BigInt=m_tt[i][-1]).TS_OFF_BigInt:          
            r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 00:00:00"
            r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
            if round(datet.datetime.timestamp(r_p)) not in m_tt[i]:
                m_tt[i].append(round(datet.datetime.timestamp(r_p)))

            r_tt = datetime.fromtimestamp(m_tt[i][-1]).strftime("%Y %m %d") + " 23:59:59"
            r_pp = datet.datetime.strptime(r_tt,'%Y %m %d %H:%M:%S')
            if round(datet.datetime.timestamp(r_pp)) not in m_tt[i]:
                m_tt[i].append(round(datet.datetime.timestamp(r_pp)))
            print(m_tt[i])

        # elif(sorted(m_tt[i])[0] in [i.TS_OFF_BigInt for i in a_id]) and (sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]) and not Alerts.objects.get(GFRID=gfrid,alertNotify_id=9,TS_BigInt=m_tt[i][-1]).TS_OFF_BigInt:                
        #     r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 00:00:00"
        #     r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
        #     if round(datet.datetime.timestamp(r_p)) not in m_tt[i]:
        #         m_tt[i].append(round(datet.datetime.timestamp(r_p)))

        #     if round(round(datetime.now().replace(tzinfo=timezone.utc).timestamp())) not in m_tt[i]:
        #         m_tt[i].append(round(datetime.now().replace(tzinfo=timezone.utc).timestamp()))

        elif(sorted(m_tt[i])[0] in [i.TS_OFF_BigInt for i in a_id]):
            # #print(m_t[t][i][0])
            r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 00:00:00"
            r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
            if round(datet.datetime.timestamp(r_p)) not in m_tt[i]:
                m_tt[i].append(round(datet.datetime.timestamp(r_p)))

        elif(sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]):  
            print(i,m_tt[i][-1])           
            r_t = datetime.fromtimestamp(m_tt[i][-1]).strftime("%Y %m %d") + " 23:59:59"
            r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
            if round(datet.datetime.timestamp(r_p)) not in m_tt[i]:
                m_tt[i].append(round(datet.datetime.timestamp(r_p)))

        elif(sorted(m_tt[i])[-1] in [i.TS_BigInt for i in a_id]):
            print(i,len(m_tt)-1) 
            if round(round(datetime.now().replace(tzinfo=timezone.utc).timestamp())) not in m_tt[i]:
                m_tt[i].append(round(datetime.now().replace(tzinfo=timezone.utc).timestamp()))
            
        elif(m_tt[i][0] in ts_list):                
            r_t = datetime.fromtimestamp(m_tt[i][0]).strftime("%Y %m %d") + " 23:59:59"
            r_p = datet.datetime.strptime(r_t,'%Y %m %d %H:%M:%S')
            if round(datet.datetime.timestamp(r_p)) not in m_tt[i]:
                m_tt[i].append(round(datet.datetime.timestamp(r_p)))
    print(m_tt)
    main_t = []
    main_tt = []
    main_y = []
    for i in range(len(ts_list)):
        main_t.append([])
        main_tt.append([])
        main_y.append([])

    for i in range(len(m_tt)):
        if len(m_tt[i])!=0:
            main_t[i].extend(sorted(m_tt[i]))
        else:
            main_t[i].extend(m_tt[i])

    # Iterate through the list and create pairs
    for i in range(len(main_t)):
        for j in range(0, len(main_t[i]), 2):
            if len(main_t[i]) == 0:
                main_tt[i].append([0,0])
            if j + 1 < len(main_t[i]):
                gmt_timezone = pytz.timezone('GMT')
                if float(datetime.fromtimestamp(main_t[i][j], gmt_timezone).strftime("%-H.%M")) != float(datetime.fromtimestamp(main_t[i][j+1], gmt_timezone).strftime("%-H.%M")):
                    main_tt[i].append([float(datetime.fromtimestamp(main_t[i][j], gmt_timezone).strftime("%-H.%M")),float(datetime.fromtimestamp(main_t[i][j+1], gmt_timezone).strftime("%-H.%M"))])

    for i in range(len(main_tt)):
        if len(main_tt[i]) == 0:
            main_y[i].append([0,0])
        for j in range(len(main_tt[i])):
            main_y[i].append(main_tt[i][j]) 

    print(main_y)

    main_yy = main_y

    res = {}
    for key in day_list:
        for value in main_yy:
            res[key] = value
            main_yy.remove(value)
            break
    print(res)

    bar_traces = []

    # Iterate through each day's usage time ranges
    for day, ranges in res.items():
        for start_hour, end_hour in ranges:
            usage_duration = end_hour - start_hour

            hover_text = f"Day: {day} Start: {start_hour} End: {end_hour} Duration: {round(usage_duration,2)} hrs"

            # Create a bar trace for the specified usage hours
            bar_trace = go.Bar(
                x=[day],
                y=[usage_duration],
                base=start_hour,
                hoverinfo='text',
                text=hover_text,
                marker=dict(color='blue'),
                showlegend=False,                   
            )

            bar_traces.append(bar_trace)

    # Create a Plotly layout
    layout = go.Layout(
        margin=dict(l=0, r=20, b=0, t=20, pad=0),
        xaxis=dict(showgrid=False, zeroline=False, showticklabels=True),
        yaxis=dict(showgrid=True, showticklabels=True),
    )

    # Create a Plotly figure with the bar traces and layout
    
    # type='category',
    # categoryorder='array',
    # categoryarray=list(res.keys()),

    fig = go.Figure(data=bar_traces, layout=layout)
    fig.update_xaxes(title="Days",tickmode='linear', ticklabelmode="period")
    fig.update_yaxes(fixedrange=False,automargin=True,range=[0,24],title="Hour")
    # fig.show()

    # Show the Plotly figure
    plot_div = opy.plot(fig, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
    # Create a figure and axis
    # fig, ax = plt.subplots()

    # # Iterate through each day's usage time ranges
    # for day, ranges in res.items():
    #     for start_hour, end_hour in ranges:
    #         usage_duration = end_hour - start_hour
    #     # #print(usage_duration)
        
    #         # Plot a bar for the specified usage hours
    #         bar = ax.bar(day, usage_duration, bottom=start_hour, color='blue')

    #         tooltip_text = f"{round(usage_duration,2)} hrs"
    #         if tooltip_text != "0 hrs":
    #             ax.annotate(tooltip_text, xy=(day, end_hour),fontsize=4)

    # # Set y-axis range to 24 hours
    # ax.set_ylim(0, 24)
    # ax.xaxis.set_major_locator(plt.MaxNLocator(10))
    # plt.xticks(rotation=45)

    # # buf = io.BytesIO()
    # # fig.tight_layout()
    # # fig.savefig(buf,format='png')
    # # buf.seek(0)
    # # string = base64.b64encode(buf.read())
    # # uri = "data:image/png;base64," + urllib.parse.quote(string)

    # # x = day_list
    # # y = main_y

    # # # Create a Plotly Image trace (Not recommended for images; for illustration purposes only)
    # # trace = go.Image(
    # #     source=uri,
    # #     hoverinfo='none'
    # # )

    # trace = go.Bar(
    #     source=plt,
    #     hoverinfo='none'
    # )

    # # # Create a Plotly layout with an image added to it
    # layout = go.Layout(
    #     margin=dict(l=0, r=0, b=0, t=0, pad=0),
    #     images=[dict(
    #         source=uri,
    #         x=0,  # X-coordinate (left)
    #         y=1,  # Y-coordinate (top)
    #         xref='paper',  # Reference relative to the paper
    #         yref='paper',
    #         opacity=1,  # Opacity of the image
    #         layer='below'
    #     )],
    #     autosize=True,
    #     xaxis=dict(showgrid=False, zeroline=False,showticklabels=False),
    #     yaxis=dict(showgrid=False, zeroline=False,showticklabels=False),
    # )
    
    # # Create a Plotly figure with the Image trace and layout
    # figg = go.Figure(data=[trace], layout=layout)
    # # figg = px.imshow(uri)

    # # # Show the Plotly figure
    # plot_div = opy.plot(figg, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False})
    return render(request,'viewdetailM.html',{'grph':plot_div,'vehicle':veh,'l_on':l_on,'l_com':l_com,'l_cp':l_cp,'ty':ty,'dt':f"Selected date range  :  {dt_range}"})

def Pie_chart(request):        
    labels = ['Balers', 'Mini Skid Steers', 'Pipeline Trenchers', 'Stump Cutters', 'Pedestrian Trenchers', 'Pile Driver']
    values = [4, 6, 5, 2, 7, 6]

    
    colors = [
    'rgb(78, 200, 111)',    # Light Green
    'rgb(146, 228, 124)',   # Lighter Green
    'rgb(255, 247, 124)',   # Light Yellow
    'rgb(255, 222, 143)',   # Lighter Yellow
    'rgb(255, 179, 102)',   # Light Orange
    'rgb(255, 152, 109)'    # Lighter Orange
]

    fig = go.Figure(data=[go.Pie(labels=labels, values=values, marker=dict(colors=colors), insidetextorientation='radial')])

    fig.update_layout(
        autosize=True,  # Automatically adjust the size to fit the content
        margin=dict(l=0, r=0, b=0, t=40),  # Adjust margin for better mobile display
    )
    
    fig.update_traces(textinfo='none')

    # Plotly configuration
    plot_div = opy.plot(fig, auto_open=False, output_type='div', config={'modeBarButtonsToRemove': ['zoom', 'pan'], 'displaylogo': False, 'displayModeBar': False})

    mydict = create_dict()
    mydict.add("piechart", {"pie": plot_div})

    return render(request, 'pie_chart.html', context=mydict)

def LiveMapv(request):
    ls = ['JIVSJIVERVME3693','JIVSJIVERVME9140','JIVSJIVERVME7124']
    data = []
    for t in range(len(ls)):                
        cust_tracker = ls[t]
        g_id = GFRID_UPS.objects.get(tracker=cust_tracker).id
        ver = GFRID_UPS.objects.get(tracker=cust_tracker).version_id
        ver_code = (Version_UPS.objects.get(id=ver).code).lower()
        sensor_code = "gpsl86"
        with connections['ups'].cursor() as cursor1:
            cursor1.execute(f"SELECT * FROM {ver_code}_{sensor_code} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
            da = cursor1.fetchall()
        # print(da)
        data.append(da[0])

    m = folium.Map(location=[13.0827,80.2707], width="%100", height="100%",zoom_start=10, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
    for row in data:
        trac = GFRID_UPS.objects.get(id=row[1]).tracker
        if trac == "JIVSJIVERVME3693":
            cust_trac = 'JIVMOT0001B0001'
            veh = "BL0183"
        elif trac == "JIVSJIVERVME9140":
            cust_trac = 'JIVMOT0002B0002'
            veh = "SC1823"
        elif trac == "JIVSJIVERVME7124":
            cust_trac = 'JIVMOT0003B0003'
            veh = "PT1945"
        # elif trac == "JIVSJILDVLCS6123":
        #     cust_trac = 'JIVMOT0004B0004'
        #     veh = "MS0123"

        # lc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%Y-%m-%d %H:%M:%S")
        # cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
        # cur = datet.datetime.strptime(cur,'%Y-%m-%d %H:%M:%S')
        # lc_ts = datet.datetime.strptime(lc_ts,'%Y-%m-%d %H:%M:%S')
        # loc_ts = timeago.format(lc_ts,cur)
        # lu.append(loc_ts)
        iframe = folium.IFrame("Lat : "+str(cust_trac))
        try:
            geolocator = Nominatim(user_agent="my_geocoder")
            location = geolocator.reverse((row[3].strip()[:-1],row[4].strip()[:-1]), exactly_one=True)
            address = location.raw.get("address", {})
            loc_city = address.get('state','')
            loc_pin = address.get('postcode','')
            l_cp = str(loc_city)+"," +str(loc_pin)
            # lua.append(l_cp)
        except GeocoderServiceError as e:
            #print("Geocoder Service Error:", e)
            return None, None
        # popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=70)
        tooltip = "Location :"+str(location).replace(", ", ", <br>") +"<br>Vehicle :"+str(veh)
        folium.Marker(([row[3].strip()[:-1],row[4].strip()[:-1]]),tooltip=tooltip).add_to(m)

    ms=m._repr_html_()
    mydict = create_dict()
    mydict.add("folium",({"map":ms}))
    return render(request, 'mapv.html',context=mydict)

def Unlock(request):
    if TrackerCommands_ver.objects.get(id=89).process_flag == 0:
        tc = TrackerCommands_ver.objects.filter(id=89).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=133).process_flag == 0:
        # return HttpResponse("In process")    
    return redirect("mac")

def Lock(request):
    if TrackerCommands_ver.objects.get(id=90).process_flag == 0:
        tc = TrackerCommands_ver.objects.filter(id=90).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=134).process_flag == 0:
        # return HttpResponse("In process")
    return redirect("mac")

def Unlock1(request):
    if TrackerCommands_ver.objects.get(id=95).process_flag == 0:
        tc = TrackerCommands_ver.objects.filter(id=95).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=155).process_flag == 0:
        # return HttpResponse("In process")    
    return redirect("mac")

def Lock1(request):
    if TrackerCommands_ver.objects.get(id=96).process_flag == 0:
        tc = TrackerCommands_ver.objects.filter(id=96).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=156).process_flag == 0:
        # return HttpResponse("In process")
    return redirect("mac")

def Unlock2(request):
    if TrackerCommands_ver.objects.get(id=83).process_flag == 0:
        tc = TrackerCommands_ver.objects.filter(id=83).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=155).process_flag == 0:
        # return HttpResponse("In process")    
    return redirect("mac")

def Lock2(request):
    if TrackerCommands_ver.objects.get(id=84).process_flag == 0:
        tc = TrackerCommands_ver.objects.filter(id=84).update(tester_flag=1)
    # while TrackerCommands.objects.get(id=156).process_flag == 0:
        # return HttpResponse("In process")
    return redirect("mac")

############################################################################################################################

#########################################Vehicle################################################################

def Amb(request):
    ls = ['JIVSJILDVGFR7034','JIVSJILDVGFR9804']
    mydict = create_dict()
    data = []
    oxy = []
    for t in range(len(ls)):
        oxy.append([])
    
    for t in range(len(ls)):
        cust_tracker = ls[t]
    
        global subs_start
        global subs_end
        global Olevel
        
        subs_start = 1669899600
        subs_end = 1806083768

        exst = GFRID_UPS.objects.filter(tracker=cust_tracker).exists()
        g_id = GFRID_UPS.objects.get(tracker=cust_tracker).id

        if exst is True:
            ts_now = round(datetime.now().replace(tzinfo=timezone.utc).timestamp())
            if ts_now < subs_end:
                ver_code = "ldv"
                sensor_code1 = "gpsl86"

                # mydict = create_dict()
#                 if "gpsl86" in sensor_code and "lcell711_1" in sensor_code
                with connections['ups'].cursor() as cursor1:
                    cursor1.execute(f"SELECT * FROM {ver_code}_{sensor_code1} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
                    da = cursor1.fetchall()
                    data.append(da[0])
                
                mydict.add("Customer",({"tracker":cust_tracker}))

                sensor_code2 = "lcell711_1"
                with connections['ups'].cursor() as cursor2:
                    cursor2.execute(f"SELECT * FROM {ver_code}_{sensor_code2} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
                    data2 = cursor2.fetchall()

                cur = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')

                for row in data2:
                    loc_ts = datetime.fromtimestamp(int(row[6]),tz.tzutc()).strftime("%Y-%m-%d %H:%M:%S")
                    oxy[t].append(row[3])
                    oxy[t].append(timeago.format(loc_ts,cur))
                mydict.add("oxy",({"oxy":oxy}))

                # print(cust_tracker)
                # mylist = []cust_trac
                # print(data)
                # if sensor_code[table]=="gpsl86" and "lcell711_1":
    m = folium.Map(location=[13.0827,80.2707],zoom_start=5, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)
    loc_ts = []
    time_ago = []
    print(data)
    for row in data:
        trac = GFRID_UPS.objects.get(id=row[1]).tracker
        print(trac)
        if trac == "JIVSJILDVGFR7034":
            cust_trac = 'JIVMOT0001B0001'
            vehicle = 'Tavera - xx xx xxxx'
        # elif(trac == "JIVSJILDVLCS3703"):
            # cust_trac = 'JIVMOT0002B0002'
        elif(trac == "JIVSJILDVGFR9804"):
            cust_trac = 'JIVMOT0003B0003'
            vehicle = 'Bolero - xx xx xxxx'
        # elif(trac == "JIVSJILDVLCS6123"):
            # cust_trac = 'JIVMOT0004B0004'
        # elif(trac == "JIVSJILDVLCS7597"):
        #     cust_trac = 'JIVMOT0005B0005'
        #     vehicle = 'EECO - OD 67 1029'
        loc_ts.append(datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%B %d, %Y, %H:%M:%S %p"))      
        lc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime('%Y-%m-%d %H:%M:%S')
        cur_time = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
        time_ago.append(timeago.format(lc_ts,cur_time))      
        # iframe = folium.IFrame("Lat : "+str(cust_trac) +"<br>TS :"+str(l_ts))
        # popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=70)
        # points = [(20.258583, 85.841682)]
        # icon_url = "http://164.52.200.223:8004/static/SuperAdmin/dist/img/marker-icon-2x.png"  
        # iicon = request.scheme + "://"+request.META['HTTP_HOST'] + settings.STATIC_URL
        # iconurl = str(iicon) +"trackers/dist/img/marker-icon-2x.png"               		
        # icon = folium.features.CustomIcon(iconurl, icon_size=(20, 30))
        # print(row[3].strip()[:-1],row[4].strip()[:-1])
        geolocator = Nominatim(user_agent="my_application")
        location = geolocator.reverse((row[3].strip()[:-1],row[4].strip()[:-1]), exactly_one=True)
        tooltip = "<strong>LOCATION:</strong> "+str(location).replace(", ", ", <br>") +"<br><strong>VEHICLE :</strong>"+str(vehicle)
        folium.Marker(([row[3].strip()[:-1],row[4].strip()[:-1]]),tooltip=tooltip).add_to(m)
        # folium.Marker([20.258583, 85.841682], popup=popup).add_to(m)
        # folium.PolyLine(points, color='green', dash_array='10').add_to(m)
        
    ms=m._repr_html_()
    
    mydict.add("folium",({"map":ms,"ts":loc_ts,"t_a":time_ago}))
    # print(mydict)
    return render(request, 'indexA.html',context=mydict)

def ViewLive(request,tracker):
    cust = tracker
    print(cust)
    if cust == "JIVMOT0001B0001":
        cust_tracker = 'JIVSJILDVGFR7034'
        vechicle = 'Tavera - xx xx xxxx'
        Olevel = '82.4'
        t = '5 minutes ago'
        
    elif(cust == "JIVMOT0003B0003"):
        cust_tracker = 'JIVSJILDVGFR9804'
        vechicle = 'Bolero - xx xx xxxx'
        Olevel = '10.9'
        t = '2 Minutes Ago'
        
    # elif(cust == "JIVMOT0003B0003"):
    #     cust_tracker = 'JIVSJILDVLCS5221'
    #     vechicle = 'Bolero - xx xx xxxx'
    #     Olevel = '10.9'
    #     t = '2 Minutes Ago'
    # elif(cust == "JIVMOT0004B0004"):
    #     cust_tracker = 'JIVSJILDVLCS6123'
    #     vechicle = 'TN 07 0965'
    #     Olevel = '10.9'
    #     t = '2 Minutes Ago'
    # elif(cust == "JIVMOT0005B0005"):
    #     cust_tracker = 'JIVSJILDVLCS7597'
    #     vechicle = 'EECO - OD 67 1029'
    #     Olevel = '64.1'
    #     t = '3 Hours Ago'

    global subs_start
    global subs_end
    
    subs_start = 1669899600
    subs_end = 1899628740

    exst = GFRID_UPS.objects.filter(tracker=cust_tracker).exists()
    g_id = GFRID_UPS.objects.get(tracker=cust_tracker).id
    if exst is True:
        ts_now = round(datetime.now().replace(tzinfo=timezone.utc).timestamp())
        if ts_now < subs_end:
            ver = GFRID_UPS.objects.get(tracker=cust_tracker).version_id
            ver_code = (Version_UPS.objects.get(id=ver).code).lower()

            with connections['ups'].cursor() as cursor:
                cursor.execute(f"select * from {ver_code}_statusdata where gfrid = {g_id} AND poststatus = 'ACTIVE'")
                row = cursor.fetchall()
            # s1_sensor = []
            # for ss in row:
            #     s1_sensor.append(ss[2])
            # print(s1_sensor)
            
            sensor_code = ["gpsl86","lcell711_1"]
            # for num in range(len(s1_sensor)):
            #     sensor_code.append(str(Sensor.objects.get(id=s1_sensor[num]).code).lower())
            # print(sensor_code)

            mydict = create_dict()
        
            if "gpsl86" in sensor_code and "lcell711_1" in sensor_code:
                for table in range(len(sensor_code)):
                    with connections['ups'].cursor() as cursor1:
                        cursor1.execute(f"SELECT * FROM {ver_code}_{sensor_code[table]} WHERE gfrid = {g_id} ORDER BY last_modified DESC LIMIT 1")
                        data = cursor1.fetchall()
                    # mydict.add("Customer",({"tracker":cust, "vehicle":vechicle}))
                    mydict.add("Customer",({"tracker":cust, "vehicle":vechicle,"Olevel":Olevel, "t":t}))                    
                    # if sensor_code[table]=="gpsl86" and "lcell711_1":
                    if sensor_code[table]=="gpsl86":
                        l = []
                        for row in data:
                            m = folium.Map(location=[row[3].strip()[:-1],row[4].strip()[:-1]], width="%100", height="%100",zoom_start=13, scrollWheelZoom=True, tiles="OpenStreetMap",attributionControl=False)                          
                            loc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime("%b %d, %Y, %H:%M:%S %p")       
                            lc_ts = datetime.fromtimestamp(int(row[5]),tz.tzutc()).strftime('%Y-%m-%d %H:%M:%S')
                            cur_time = datetime.now(tz=ZoneInfo('Asia/Kolkata')).strftime('%Y-%m-%d %H:%M:%S')
                            if cust == "JIVMOT0003B0003":
                                time_ago = timeago.format(lc_ts,cur_time) 
                            else:
                                time_ago = "5 minutes ago"                
                            iframe = folium.IFrame("Lat : "+str(row[3].strip()) + '<br>' + "Lon : "+str(row[4].strip())+ "<br>TS :"+str(loc_ts))
                            popup = folium.Popup(iframe, min_width=200, max_width=300,  max_height=100)
                            # points = [(20.258583, 85.841682)]
                            # icon_url = "http://164.52.200.223:8004/static/SuperAdmin/dist/img/marker-icon-2x.png"  
                            # iicon = request.scheme + "://"+request.META['HTTP_HOST'] + settings.STATIC_URL
                            # iconurl = str(iicon) +"trackers/dist/img/marker-icon-2x.png"               		
                            # icon = folium.features.CustomIcon(iconurl, icon_size=(20, 30))
                            
                            folium.Marker([row[3].strip()[:-1],row[4].strip()[:-1]], popup=popup, icon = folium.Icon(color='blue', icon="info-sign")).add_to(m)
                            
                            # folium.PolyLine(points, color='green', dash_array='10').add_to(m)
                            
                            geolocator = Nominatim(user_agent="trackers")
                            ll = geolocator.reverse(row[3].strip()[:-1]+","+row[4].strip()[:-1])
                            # address = location.raw['address']
                            # suburb =address.get('suburb','')
                            # city = address.get('city', '')
                            # state = address.get('state', '')
                            mydict.add("Location",({"lat":row[3],"lon":row[4],"dt":loc_ts,"t_a":time_ago}))
                            # print(location.address)
                            # l.append(location.address)
                            # ll = [x for xs in l for x in xs.split(',')][4:]
                            print(ll)
                            mydict.add("Loc",({"f_a":ll}))
                        print(mydict) 
                        ms=m._repr_html_()
                        mydict.add("folium",({"map":ms}))

                    elif sensor_code[table]=="lcell711_1":
                        for row in data:                            
                            loc_ts = datetime.fromtimestamp(int(row[6]),tz.tzutc()).strftime("%b %d, %Y, %H:%M:%S")
                            
                            mydict.add("Loadcell",({"perwght":row[3],"dt":loc_ts})) 
                # stud_json = json.dumps(mydict,default=str)
                # return HttpResponse(mydict)
                print(cust_tracker)
                return render(request,'viewlive.html',context=mydict)

################################################################################################################