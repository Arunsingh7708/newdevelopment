from django.conf.urls import url
from .views  import *

urlpatterns = [
    url(r'^$',Home,name='home'),
    url(r'^Development/',Dev,name='dev'),
    url(r'^login_auth/',login_auth,name='login_auth'),
    url(r'^OBD/',OBD,name='obd'),
    url(r'^CAN/',CAN,name='can'),
    url(r'^ROBOTARM/',RobotArm,name='robotarm'),
    url(r'^SMARTMOTOR/',SmartMotor,name='smartmotor'),
    url(r'^SMARTVALUE/',SmartValue,name='smartvalue'),
    url(r'^CAMERA/',Camera,name='camera'),
    url(r'^logout/',Logout,name='logout'),

    url(r'^ups/',Ups,name="ups"),
    url(r'^settings_page/',settings_page, name='settings_page'),
    url(r'^live_map/',Live_map, name='live_map'),
    url(r'^alerts_page/',alerts_page, name='alerts_page'),
    url(r'^specification_page/',specification_page, name='specification_page'),
    url(r'^alerts_Status/(?P<upsid>\w+)',alert_Status,name='alerts_Status'),
    url(r'^upsdetail/(?P<upsid>\w+)',UpsDetail,name='upsdetail'),

    url(r'^TrackerList/',Cold,name='cold'),
    url(r'^SensorList/(?P<tracker>\w+)',SensorD,name='sensor'),

    url(r'^Machinery/',Mac,name="mac"),
    url(r'^Machinerydetail/(?P<tracker>\w+)',ViewLiveM,name='viewdetailM'),
    url(r'^livemap/',LiveMapv, name='live_mapv'),
    url(r'^pie_chart/',Pie_chart, name='pie_chart'),
    url(r'^Lock/',Lock,name="Lock"),
    url(r'^UnLock/',Unlock,name="UnLock"),
    url(r'^Lock1/',Lock1,name="Lock1"),
    url(r'^UnLock1/',Unlock1,name="UnLock1"),
    url(r'^Lock2/',Lock2,name="Lock2"),
    url(r'^UnLock2/',Unlock2,name="UnLock2"),

    url(r'^Ambulance/',Amb,name="amb"),
    url(r'^viewlive/(?P<tracker>\w+)',ViewLive,name='viewlive'),
]