from unittest.util import _MAX_LENGTH
from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, AbstractBaseUser

class Version(models.Model):
    version = models.CharField(max_length=50, unique = True)
    code = models.CharField(max_length = 3,  unique = True)
    sensors = models.CharField(max_length=200,default="")  
    # Json_File =  models.FileField(upload_to='files/jsonFiles', blank=False, validators=[validate_file_extension])
    configtabs = models.CharField(max_length=200,default="")  
    # Json_File_Config =  models.FileField(upload_to='files/jsonFiles', default="", validators=[validate_file_extension])
    # ext_Json_File =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    # ext_Json_File_Config =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    # alter_units_table =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    last_modified = models.DateTimeField(auto_now = True)
    def __str__(self):
        return u'{0}'.format(self.version)
    
    class Meta:
        app_label = 'ups'
        db_table = 'SuperAdmin_version'

class GFRID_UPS(models.Model):
    Code     = models.CharField(max_length=3)
    tracker = models.CharField(max_length=150, unique=True)
    Mfg = models.CharField(max_length=30)
    Device = models.CharField(max_length=15)
    SerialNo = models.CharField(max_length=15)
    version =  models.ForeignKey(Version, on_delete=models.CASCADE)
    SAMVer = models.CharField(max_length=150)
    SWver = models.CharField(max_length=150, default="")
    hardware_Id = models.CharField(max_length=150, unique=True)
    RECHARGE =  models.IntegerField(null=True)
    SQN =  models.IntegerField(null=True)
    config_ver = models.IntegerField(default=0)
    MOTIONDET =  models.CharField(max_length=50, default="off")
    TS = models.BigIntegerField(null=True)
    PowerSource = models.CharField(max_length=20, default="")
    PowerLineAlert = models.IntegerField(default="2")
    DataRecoInfo = models.CharField(max_length=250, default="")
    fullJSON = models.JSONField(null = True)
    fullJSON_date = models.DateTimeField(auto_now = True)
    gereq_flag = models.IntegerField(default=0)
    tracker_name = models.CharField(max_length=250, default="")  
    Encryptions = models.CharField(max_length=20, default="")  
    prefix_encryption_string = models.CharField(max_length=20, default="")   
    encryption_string = models.CharField(max_length=20, default="")   
    handshake_status = models.IntegerField(null=False,default=0)  
    capability_status = models.IntegerField(null=False,default=0)
    last_modified = models.DateTimeField(auto_now = True)

    class Meta:
        app_label = 'ups'
        db_table = 'SuperAdmin_gfrid'

class GSMSQL_UPS(models.Model):
    GFRID = models.IntegerField(null=False)
    SQValue =  models.IntegerField(default=0)
    TS = models.BigIntegerField(null=True)
    last_modified = models.DateTimeField(auto_now = True) 
    class Meta:
        app_label = 'ups'
        db_table = 'SuperAdmin_gsmsql'

class Version_UPS(models.Model):
    version = models.CharField(max_length=50, unique = True)
    code = models.CharField(max_length = 3,  unique = True)
    sensors = models.CharField(max_length=200,default="")  
    # Json_File =  models.FileField(upload_to='files/jsonFiles', blank=False, validators=[validate_file_extension])
    configtabs = models.CharField(max_length=200,default="")  
    # Json_File_Config =  models.FileField(upload_to='files/jsonFiles', default="", validators=[validate_file_extension])
    # ext_Json_File =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    # ext_Json_File_Config =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    # alter_units_table =  models.FileField(upload_to='files/jsonFiles', default="", max_length=500, validators=[validate_file_extension])
    last_modified = models.DateTimeField(auto_now = True)
    def __str__(self):
        return u'{0}'.format(self.version)

    class Meta:
        app_label = 'ups'
        db_table = 'SuperAdmin_version'

class TrackerCommands_ver(models.Model):
    tracker = models.CharField(max_length=150 )
    commandID = models.IntegerField(null=False)
    flag =  models.IntegerField(default=0)
    cmdOrder =  models.IntegerField(default=0)
    tester_flag =  models.IntegerField(default=0)
    tester_cmdOrder =  models.IntegerField(default=0)
    cust_flag =  models.IntegerField(default=0)
    cust_cmdOrder =  models.IntegerField(default=0)   
    process_flag = models.IntegerField(default=0)
    process_time = models.DateTimeField(auto_now = True)  
    last_modified = models.DateTimeField(auto_now = True) 

    class Meta:
        app_label = 'ups'
        db_table = 'SuperAdmin_trackercommands'

class AlertNotification(models.Model):
    alert_notice = models.CharField(max_length=200)
    hexVal = models.CharField(max_length=100)
    binaryVal = models.CharField(max_length=100)
    sensor = models.CharField(max_length=50)
    permission = models.CharField(max_length=50, default="")
    last_modified = models.DateTimeField(auto_now = True) 

    class Meta:
        app_label = 'ups'
        db_table = 'SuperAdmin_alertnotification'

class Alerts(models.Model):
    alert = models.CharField(max_length=150)
    alertNotify = models.ForeignKey(AlertNotification, on_delete=models.CASCADE)
    status = models.IntegerField(null=True,default=2) # 2 means grey, 1 means red,0 means green
    GFRID = models.IntegerField(null=False)
    TS = models.DateTimeField(null=False, blank=False)
    TS_OFF = models.DateTimeField(null=True, blank=True)
    TS_BigInt = models.BigIntegerField(null = True)
    TS_OFF_BigInt = models.BigIntegerField(null=True)
    jsonFile = models.JSONField(null = True)
    last_modified = models.DateTimeField(auto_now = True) 

    class Meta:
        app_label = 'ups'
        db_table = 'SuperAdmin_alerts'